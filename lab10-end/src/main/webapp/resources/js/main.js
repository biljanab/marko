$(document).ready(function() {
	
	/* Home page view */
	HomePageView = Backbone.View.extend({
		el : $('#container'), // "el" oznaÄ�ava za koji deo HTML stranice je zaduÅ¾en ovaj view, odnosno gde Ä‡e se prikazivati
		template : _.template($('#homePageView').html()), // template je u templates.jsp unutar <script type="text/template" id="homePageView"> taga
		initialize : function() { // "initialize" je konstruktor za view
			this.render();
		},
		render : function() { // "render" je funkcija za iscrtavanja view-a
			$(this.el).html(this.template()); // ubacivanje HTML-a definisanog u template unutar el elementa
		}
	});
	
	/* Activity */
	ActivityModel = Backbone.Model.extend({
		urlRoot : apiRoot + 'activities', // "urlRoot" je korenska adresa na REST API (web servisu) za ovaj entitet
		defaults : { // podrazumevane vrednosti polja, Backbone model mora da se poklapa sa DTO modelom
			id : null,
			name : ''
		}
	});
	
	ActivityCollection = Backbone.Collection.extend({
		model : ActivityModel, // ovo je kolekcija Activity modela
		url : apiRoot + 'activities' // adresa na REST API gde se nalazi ova kolekcija
	});
	
	/* Activities page view */
	ActivitiesPageView = Backbone.View.extend({
		el : $('#container'), // "el" oznaÄ�ava za koji deo HTML stranice je zaduÅ¾en ovaj view, odnosno gde Ä‡e se prikazivati
		template : _.template($('#activitiesPageView').html()), // template je u templates.jsp unutar <script type="text/template" id="activitiesPageView"> taga
		initialize : function() {
			this.render(); // iscrtaj stranicu
			this.getActivities(); // zatim preuzmi aktivnosti kako bi se iscrtale
		},
		render : function() {
			$(this.el).html(this.template()); // ubacivanje HTML-a definisanog u template unutar el elementa
		},
		getActivities : function() { // metoda koja dobavlja aktivnisti sa servera
			new ActivityCollectionView({ el : $('#activitiesTable tbody')}); // pravljenje ActivityCollectionView-a, i setovanje el elementa kroz parametre konstruktora
		}
	});
	
	/* Activity collection view */
	ActivityCollectionView = Backbone.View.extend({
		model : new ActivityCollection, // model koji Ä‡e prikazivati ovaj view je ActivityCollection
		initialize : function() {
			var self = this; // kako bi imali referencu na this unutar callback funkcije
			this.model.fetch({ // poziva se GET /api/activities
				success : function() { // callback ako je bilo 200 OK
					self.render(); // aktivnosti su dobavljene, preko self imamo referencu na this, pa moÅ¾emo da pozovemo render
				},
				error : function() { // callback ako nije bilo 200 OK

				}
			});
		},
		render : function() {
			for (var key in this.model.models) { // uzimanje aktivnosti jednu po jednu
				var activity = this.model.models[key]; // poÅ¡to je this.model.models hash, konkretnu aktivnost uzimamo po kljuÄ�u
				var activityItemView = new ActivityItemView({ model : activity }); // za tu aktivnost pravimo ActivityItemView (jedan red u tabeli), dinamiÄ�ki setujemo model kroz parametar konstruktora
				var domEl = activityItemView.render().el; // poziva se render za ActivityItemView, Å¡to izgeneriÅ¡e HTML za taj red u tabeli, i .el vraÄ‡a taj red kao DOM element
				$(this.el).append(domEl); // dodajemo red tabele unutar tbody
			}
		}
	});
	
	/* Activity item view */
	ActivityItemView = Backbone.View.extend({
		tagName : "tr", // el nije eksplicitno specificiran, veÄ‡ je samo reÄ�eno da Ä‡e ovaj view biti unutar nekog tr taga
		template : _.template($('#activityItemView').html()), // template je u templates.jsp unutar <script type="text/template" id="activityItemView"> taga
		events : { // definisanje event-ova (dogaÄ‘aja) u formatu "vrsta_eventa element" : "metoda"
			"click .removeActivity" : "removeActivity"
		},
		initialize : function() {
			
		},
		render : function() {
			$(this.el).html(this.template({ activity : this.model.toJSON() })); // generisanje HTML preko template-a, ali uz dodatni podatak - activity, koji sadrÅ¾i id i name aktivnosti
																				// this.model je zapravo aktivnost koja je postavljenja kroz parametar konstruktora (linija 62)
			return this; // vraÄ‡amo this kako bismo mogli da vratimo el ovog view-a (linija 63)
		},
		removeActivity : function() {
			this.model.destroy(); // poziva DELETE /api/activities/{id}
			$(this.el).remove(); // brisanje ovog view-a, odnosno el koji ga predstavlja iz DOM stabla
			return false; // return false zaustavlja propagaciju dogaÄ‘aja klika, odnosno poÅ¡to se removeActivity izvrÅ¡ava na klik na <a href="#activities/remove/{id}>
						  // po defaultu Ä‡e browser nakon klika da ubaci tu adresu u adress bar browsera, a zapravo Å¾elimo da ostanemo na stranici #activities
		}
	});
	
	/* Add/edit activity view */
	AddEditActivityPageView = Backbone.View.extend({
		el : $('#container'), // "el" oznaÄ�ava za koji deo HTML stranice je zaduÅ¾en ovaj view, odnosno gde Ä‡e se prikazivati
		model : null,
		template : _.template($('#addEditActivityPageView').html()), // template je u templates.jsp unutar <script type="text/template" id="addEditActivityPageView"> taga
		events : {
			"click #btnSaveActivity" : "saveActivity"
		},
		initialize : function(params) {
			if (params && params.id) { // ovo je edit stranica
				var self = this;
				this.model = new ActivityModel({ id : params.id });
				this.model.fetch({
					success : function() {
						self.render();
					},
					error : function() {
						
					}
				});
			} else { // ovo je add stranica
				this.model = new ActivityModel;
				this.render();
			}
		},
		render : function() {
			$(this.el).html(this.template({ activity : this.model.toJSON() }));
		},
		saveActivity : function() {
			var name = $('#activityName').val();
			this.model.set('name', name);
			this.model.save({}, {
				success : function() {
					appRouter.navigate('activities', true);
				},
				error : function() {
					
				}
			});
		},
		close : function () {
	        $(this.el).unbind();
	    }
	});
	
	/* Router */
	AppRouter = Backbone.Router.extend({ // Backbone router je zapravo kontroler unutar klijentske MVC aplikacije (tj. ove Backbone aplikacije)
		routes : { // definisanje ruta u formatu "ruta" : "metoda". "ruta" je u browseru zapravo #ruta
			"" : "homeRoute",
			"activities" : "activitiesRoute",
			"activities/add" : "addActivityRoute",
			"activities/edit/:id" : "editActivityRoute"
		},
		homeRoute : function() {
			new HomePageView;
		},
		activitiesRoute : function() {
			new ActivitiesPageView;
		},
		addActivityRoute : function() {
			if (appRouter.addEditActivityPageView) { appRouter.addEditActivityPageView.close(); }
		 	appRouter.addEditActivityPageView = new AddEditActivityPageView;
		},
		editActivityRoute : function(id) {
			if (appRouter.addEditActivityPageView) { appRouter.addEditActivityPageView.close(); }
		 	appRouter.addEditActivityPageView = new AddEditActivityPageView({ id : id });
		}
	});
	
	var appRouter = new AppRouter; // obavezno da bi aplikacija radila
	
	Backbone.history.start(); // omoguÄ‡ava privid navigacije kroz stranice preko hashtagova (#), iako samo zapravo stalno na jednoj stranici
});