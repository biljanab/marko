<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<h1>WAFEPA - Add/Edit user</h1>

<c:url value="/users" var="usersUrl" />

<form:form action="${usersUrl}" method="post" modelAttribute="user">
	<fieldset>
		<form:hidden path="id"/>
		<form:label path="email">Email</form:label>
		<form:input path="email" />
		<form:errors path="email" cssStyle="color:red"></form:errors>
		<br />
		
		<form:label path="password">Password</form:label>
		<form:input path="password" />
		<form:errors path="password" cssStyle="color:red"></form:errors>
		<br />
		
		<form:label path="firstname">First name</form:label>
		<form:input path="firstname" />
		<form:errors path="firstname" cssStyle="color:red"></form:errors>
		<br />
		
		<form:label path="lastname">Last name</form:label>
		<form:input path="lastname" />
		<form:errors path="lastname" cssStyle="color:red"></form:errors>
		<br />
	</fieldset>
	<p><button type="submit">Submit</button></p>
</form:form>