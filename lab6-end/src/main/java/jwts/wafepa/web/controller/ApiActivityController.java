package jwts.wafepa.web.controller;

import java.util.List;

import javax.validation.Valid;

import jwts.wafepa.model.Activity;
import jwts.wafepa.service.ActivityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/api/activities")
public class ApiActivityController {

	@Autowired
	private ActivityService activityService;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<Activity>> getActivities() {
		List<Activity> activities = activityService.findAll();
		return new ResponseEntity<>(activities, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Activity> getActivity(@PathVariable Long id) {
		Activity activity = activityService.findOne(id);
		if (activity != null) {
			return new ResponseEntity<>(activity, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Activity> deleteActivity(@PathVariable Long id) {
		Activity activity = activityService.findOne(id);
		if (activity != null) {
			activityService.remove(id);
			return new ResponseEntity<>(activity, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(consumes="application/json", method=RequestMethod.POST)
	public ResponseEntity<Activity> saveActivity(@RequestBody @Valid Activity activity) {
		activity = activityService.save(activity);
		return new ResponseEntity<>(activity, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}", consumes="application/json", method=RequestMethod.PUT)
	public ResponseEntity<Activity> editActivity(@PathVariable Long id, @RequestBody @Valid Activity activity) {
		if (activityService.findOne(id) != null) {
			if (id != activity.getId()) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			} else {
				activity = activityService.save(activity);
				return new ResponseEntity<>(activity, HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
