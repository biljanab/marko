<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<tiles:importAttribute name="title" />
<c:url value="/" var="root" />

<!DOCTYPE HTML>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title><fmt:message key="${title}" /></title>

        <!-- Bootstrap -->
        <link href="${root}resources/css/bootstrap.css" rel="stylesheet">
        <link href="${root}resources/css/bootstrap-theme.css" rel="stylesheet">
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="${root}">Home</a>
                </div>
            </div>
        </div>
        <div class="jumbotron">
            <div class="container text-center">
                <h1>WAFEPA</h1>
                <p>Web Application For Evidenting Physical Activities</p>
            </div>
        </div>

        <div class="container" style="padding: 4em">
            <tiles:insertAttribute name="content" />
        </div>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="${root}resources/js/jquery-2.1.1.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="${root}resources/js/bootstrap.js"></script>
    </body>
</html>