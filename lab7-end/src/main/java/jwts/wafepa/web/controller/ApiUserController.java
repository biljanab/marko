package jwts.wafepa.web.controller;

import java.util.List;

import javax.validation.Valid;

import jwts.wafepa.model.User;
import jwts.wafepa.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value="/api/users")
public class ApiUserController {

	@Autowired
	private UserService userService;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<User>> getActivities() {
		List<User> users = userService.findAll();
		return new ResponseEntity<>(users, HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<User> getUser(@PathVariable Long id) {
		User user = userService.findOne(id);
		if (user != null) {
			return new ResponseEntity<>(user, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<User> deleteUser(@PathVariable Long id) {
		User user = userService.findOne(id);
		if (user != null) {
			userService.remove(id);
			return new ResponseEntity<>(user, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(consumes="application/json", method=RequestMethod.POST)
	public ResponseEntity<User> saveUser(@RequestBody @Valid User user) {
		user = userService.save(user);
		return new ResponseEntity<>(user, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}", consumes="application/json", method=RequestMethod.PUT)
	public ResponseEntity<User> editUser(@PathVariable Long id, @RequestBody @Valid User user) {
		if (userService.findOne(id) != null) {
			if (id != user.getId()) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			} else {
				user = userService.save(user);
				return new ResponseEntity<>(user, HttpStatus.OK);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}
}
