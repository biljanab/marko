﻿## Lab 8 - SPA, Backbone.js

### SPA (Single Page Application)

U SPA, ceo potreban kod - HTML, JavaScript i CSS se učitaju odjednom, na jednoj stranici (otuda naziv Single Page Application).
Reload stranica se ne radi ni u kom trenutku, kao ni prebacivanje na neku drugu stranicu ununtar aplikacije - stvara se samo privid
reload-ovanja stranica i navigacije kao ostalim stranicama.
Interakcija sa SPA uključuje stalnu komunikaciju aplikacije sa (REST) web servisom u pozadini.

1. (SPA and the Single Page Myth)[http://www.johnpapa.net/pageinspa/]
2. (SPA-wiki)[http://en.wikipedia.org/wiki/Single-page_application]

Glavna ideja SPA je da se renderuju i menjaju samo oni elementi korisničkog interfejsa za koje je to potrebno.
Menjanje elemenata korisničkog interfejsa uglavnom podrazumeva intenzivnu manipulaciju HTML-om i DOM-om unutar stranice.

Postoji veliki broj JavaScript biblioteka koje služe za pravljenje SPA (Angular.js, Backbone.js, Ember.js...). U ovom projektu
će biti korišćen Backbone.js.

### Backbone.js

Backbone se koristi za pravljenje SPA, pritom u potpunosti se oslanjajući na REST web servise za obavljanje CRUD operacija nad podacima.

Backbone poznaje sledeće pojmove:

1. Model - entitet koji će prikazivati negde na stranici, odnosno u okviru nekog View-a. Model se dobavlja, pravi i menja putem REST web servisa.
2. View - je BILO KOJI DEO korisničkog interfejsa koji treba da se renderuje i koji služi za neku manipulaciju podacima.
Dakle, na jednoj logičkoj stranici može postojati više View-ova, npr.: View za samu stranicu, View za element unutar stranice - recimo tabela
View za svaki red u tabeli.
3. Collection - predstavlja kolekciju Model-a
4. Router - u principu predstavlja kontroler koji omogućava kretanje između logičkih stranica. Takođe, vodi računa o istoriji 
kretanja kroz stranice, pa je moguće raditi Back i Forward u web browseru.

----

* U src/main/webapp/resources/js dodati JavaScript fajlove backbone.js i underscore.js (ovi fajlovi se nalaze u assets direktorijumu).

* U src/main/webapp/resources/js napraviti novi fajl - main.js - ovde će biti pisan sav JavaScript kod koji se koristi u aplikaciji.

* U standardLayout.jsp dodati import underscore.js, backbone.js i main.js, kao i konfiguraciju za Underscore template (šablone):

```html
<script src="${root}resources/js/underscore.js"></script>
<script src="${root}resources/js/backbone.js"></script>
<script src="${root}resources/js/main.js"></script>

<script type="text/javascript">
	_.templateSettings = {
		interpolate: /\<\@\=(.+?)\@\>/gim,
		evaluate: /\<\@(.+?)\@\>/gim,
		escape: /\<\@\-(.+?)\@\>/gim
	};
</script>
```

* U direktorijumu WEB-INF/jsp napraviti novi fajl templates.jsp. U ovom fajlu će se nalaziti svi template-i za view-ove.

* Iz standardLayout.jsp izbrisati:
```html
<tiles:insertAttribute name="content" />
```
element. DIV tag sa klasom "container" izmeniti da mu i id bude "container".
Dodati:
```html
<tiles:insertAttribute name="templates" />
```
ali odmah nakon otvarajućeg body taga.

* U tiles-def/tiles.xml izbrisati definicije svih view-ova osim home view-a. Takođe, izbrisati korišćenje atributa content i dodati korišćenje atributa templates.

----

* Prebaciti sadržaj home.jsp stranice u templates.jsp stranicu. Izmeniti da se ne koriste JSP i JSTL tagovi, već obični HTML tagovi.
Takođe sva href polja izmeniti da na početku sadrže tarabu - hashtag(#).

* Prebačeni sadržaj okviriti <script type="text/template" id="homePageView"></script> elementom.

* U main.js dodati View za home stranicu i dodati mapiranje za home view u Router-u.

* Po uzoru na home view, isto uraditi za activities view.

----

* U standardLayout.jsp pre html taga dodati:

```html
<c:url value="/" var="root" />
```

i u head elementu:

```html
<script type="text/javascript">
	var apiRoot = ${root} + 'api/';
</script>
```

* Nakon dodavanja ovih elemenata, može se pristupati baznoj adresi REST web servisa.

* Dodati Model za aktivnosti -> ActivityModel i Collection za aktivnosti -> ActivityCollection.

* Dodati View-ove za ActivityCollection i ActivityModel
