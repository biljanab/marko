<%@ include file="/WEB-INF/jsp/common/tagLibs.jsp" %>

<h1>WAFEPA - Add/Edit user</h1>

<c:url value="/users" var="usersUrl" />

<form:form action="${usersUrl}" method="post" modelAttribute="user" class="form-horizontal">

	<form:hidden path="id"/>
		
	<div class="form-group">
        <form:label path="email" cssClass="col-sm-2">Email </form:label>
        <div class="col-sm-6">
            <form:input path="email" cssClass="form-control" />
        </div>
        <div class="col-sm-4">
            <span class="label label-danger"><form:errors path="email" /></span>
        </div>
    </div>
    
    <div class="form-group">
        <form:label path="password" cssClass="col-sm-2">Password </form:label>
        <div class="col-sm-6">
            <form:input path="password" cssClass="form-control" />
        </div>
        <div class="col-sm-4">
            <span class="label label-danger"><form:errors path="password" /></span>
        </div>
    </div>
    
    <div class="form-group">
        <form:label path="firstname" cssClass="col-sm-2">First name </form:label>
        <div class="col-sm-6">
            <form:input path="firstname" cssClass="form-control" />
        </div>
        <div class="col-sm-4">
            <span class="label label-danger"><form:errors path="firstname" /></span>
        </div>
    </div>
    
    <div class="form-group">
        <form:label path="lastname" cssClass="col-sm-2">Last name </form:label>
        <div class="col-sm-6">
            <form:input path="lastname" cssClass="form-control" />
        </div>
        <div class="col-sm-4">
            <span class="label label-danger"><form:errors path="lastname" /></span>
        </div>
    </div>

	<p><button type="submit">Submit</button></p>
</form:form>