package jwts.wafepa.service;

import java.util.List;

import jwts.wafepa.model.User;

public interface UserService {

	User findOne(Long id);
	List<User> findAll();
	User save(User user);
	void remove(Long id) throws IllegalArgumentException;
}
